/* global WebFontConfig : true */

var Core = Core || {};

Core = {
    constructor : function () {
        this.bodyTag = $('body');
        this.defaultPage = this.bodyTag.find('.default-page');
        this.directAccess = this.bodyTag.find('.direct-access');
        this.vars = this.getUrlVars().string;
        this.fontChoice = this.getUrlVars().fontPicker;
        this.fontStyle = this.getUrlVars().fontStyle;
        this.fontColor = this.getUrlVars().fontColor;
        this.tag = $('<h1></h1>');
        this.inputField = this.bodyTag.find('input[type="text"]');
        this.fontPicker = this.bodyTag.find('select[name="fontPicker"]');
        this.submitBtn = this.bodyTag.find('input[type="submit"]');
        this.pageHeight = this.bodyTag.height();
        this.pageWidth = this.bodyTag.width();
    },

    init : function () {
        var o = this;
        o.constructor();
        o.detectPageType();
        o.validate();
        o.replaceColourPicker();
        //As js is enabled, remove the no-js warning
        o.bodyTag.find('.notify').remove();


    },
    getUrlVars : function () {
        // Thanks to Roshambo for this method
        // http://snipplr.com/users/Roshambo/
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    defaultVars : function () {
        var o = this;
        o.defaultValue = "Type the text you want to be biggerer ";
        o.inputField.val(o.defaultValue).focus( function() {
            $(this).val('');
        });
        o.inputField.val(o.defaultValue).blur( function() {
            if (!$(this).val()) {
                $(this).val(o.defaultValue);
            }
        });
    },
    applyOptions: function() {
        var o = this;
        if (o.fontChoice) o.loadFonts(o.fontChoice, o.fontChoice.replace('+', ' '));
        if (o.fontStyle) o.tag.addClass(o.fontStyle);
        if (o.fontColor) o.tag.css('color', '#'+o.fontColor);
    },
    detectPageType : function () {
        var o = this;
        if (o.vars) {
            // The page has been accessed via a query string so display the biggerized text
            o.defaultPage.remove();
            o.vars = o.vars.replace(/\+/g, ' ');
            o.tag.text(decodeURIComponent(o.vars)).appendTo(o.directAccess);
            o.applyOptions();
            o.fitToParent();
        } else {
            // The page has been accessed without a query string. Display the default page content
            o.directAccess.remove();
            o.defaultVars();
        }
    },

    validate : function () {
        var o = this;

        o.submitBtn.bind('click', function(e) {
            var error = $('<div class="error"></div>'),
                message = '',
                valid = false;

            if (o.inputField.val().trim() === '') {
                message = 'You want me to make NOTHING bigger? Were you dropped on your head as a child?';
            } else if (o.inputField.val() === o.defaultValue) {
                message = 'I\'m not making the default text bigger, that\'s just boring';
            } else {
                valid = true;
            }

            if (!valid) {
                e.preventDefault();
                o.defaultPage.find('.error').remove();
                error.insertAfter(o.submitBtn).text(message);
            }
        });
    },

    loadFonts : function (familyName, fontName) {
        fontName = fontName || familyName; // If the font name is the same as the family name, no need to set it twice.
        var o = this;
        // Google fonts JavaScript API
        WebFontConfig = {
            google: { families: [ familyName ] }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' === document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
        if (o.vars) o.tag.css('font-family', fontName +'\'sans-serif');
    },

    fitToParent : function () {
        var o = this,
            container = o.tag.parent(),
            padding = parseInt(container.css('padding'));

       if (!/\s/.test(o.tag.text())) o.tag.css('word-break', 'break-all'); // Text has no spaces so allow word breaks

        container.css({
            height : o.pageHeight - (padding*2) + 'px',
            width : o.pageWidth - (padding*2) + 'px'
        });
        // if (o.pageWidth > 450) {
        //     // is desktop
        //     if (o.tag.text().length < 4) {
        //         o.tag.fitText(1, { minFontSize: '600px' });
        //     } else if (o.tag.text().length < 7) {
        //         o.tag.fitText(1, { minFontSize: '500px' });
        //     } else if (o.tag.text().length < 8) {
        //         o.tag.fitText(1, { minFontSize: '350px' });
        //     } else if (o.tag.text().length < 13) {
        //         o.tag.fitText(1, { minFontSize: '250px' });
        //     } else {
        //         o.tag.fitText(0.9);
        //     }
        // } else {
        //     // is mobile
        //     if (o.tag.text().length > 5) {
        //         o.tag.fitText(1, { MaxFontSize: '100px' });
        //     } else {
        //         o.tag.fitText(0.2, { MinFontSize: '100px' });
        //     }
        // }
        if (o.directAccess.length) {
            o.bodyTag.flowtype({
                fontRatio : 30
            });
        }
    },
    replaceColourPicker : function () {
        var o = this;
        if(true) {
            var oldPicker = o.bodyTag.find('select#fontColor');
            $('<input type="color" id="fontColor" name="fontColor" />').insertAfter(oldPicker)
            oldPicker.remove();

        }
    }
};

/*
* FlowType.JS v1.1
* Copyright 2013-2014, Simple Focus http://simplefocus.com/
*
* FlowType.JS by Simple Focus (http://simplefocus.com/)
* is licensed under the MIT License. Read a copy of the
* license in the LICENSE.txt file or at
* http://choosealicense.com/licenses/mit
*
* Thanks to Giovanni Difeterici (http://www.gdifeterici.com/)
*/

(function($) {
   $.fn.flowtype = function(options) {

// Establish default settings/variables
// ====================================
      var settings = $.extend({
         maximum   : 9999,
         minimum   : 1,
         maxFont   : 9999,
         minFont   : 1,
         fontRatio : 35
      }, options),

// Do the magic math
// =================
      changes = function(el) {
         var $el = $(el),
            elw = $el.width(),
            width = elw > settings.maximum ? settings.maximum : elw < settings.minimum ? settings.minimum : elw,
            fontBase = width / settings.fontRatio,
            fontSize = fontBase > settings.maxFont ? settings.maxFont : fontBase < settings.minFont ? settings.minFont : fontBase;
         $el.css('font-size', fontSize + 'px');
      };

// Make the magic visible
// ======================
      return this.each(function() {
      // Context for resize callback
         var that = this;
      // Make changes upon resize
         $(window).resize(function(){changes(that);});
      // Set changes on load
         changes(this);
      });
   };
}(jQuery));

$( function() {
    Core.init();
});
